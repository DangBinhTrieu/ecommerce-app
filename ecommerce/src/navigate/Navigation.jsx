import React from "react";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import Account from "../pages/account/Account";
import Notification from "../pages/account/components/Notification";
import UserInformation from "../pages/account/components/UserInformation";
import Cart from "../pages/cart/Cart";
import Home from "../pages/home/Home";
import Index from "../pages/home/Index";
import ErrorPage from "../router/ErrorPage";

// const router = createBrowserRouter([
//   createRoutesFromElements(
//     <Router
//       path="/"
//       element={<Root />}
//       // loader={rootLoader}
//       // action={rootAction}
//       errorElement={<ErrorPage />}
//     >
//       <Route errorElement={<ErrorPage />}>
//         <Route index element={<Index />} />
//         <Route
//           path="contacts/:contactId"
//           element={<Contact />}
//           loader={contactLoader}
//           action={contactAction}
//         />
//         <Route
//           path="contacts/:contactId/edit"
//           element={<EditContact />}
//           loader={contactLoader}
//           action={editAction}
//         />
//         <Route path="contacts/:contactId/destroy" action={destroyAction} />
//       </Route>
//     </Router>
//   ),
// ]);
const router = createBrowserRouter([
  {
    path: "/",
    element: <Index />,
    errorElement: <ErrorPage />,
    children: [
      {
        path: "/",
        element: <Home />,
      },
      {
        path: "cart/",
        element: <Cart />,
      },
      {
        path: "account/",
        element: <Account />,
        children: [
          {
            path: "user-infor/",
            element: <UserInformation />,
          },
        ],
      },
      {
        path: "account/",
        element: <Account />,
        children: [
          {
            path: "notification/",
            element: <Notification />,
          },
        ],
      },
    ],
  },
]);

function Navigation(props) {
  return <RouterProvider router={router} />;
}

export default Navigation;
