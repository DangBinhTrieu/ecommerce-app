import React from "react";
import PropTypes from "prop-types";
import "./Cart.css";
import Header from "../../components/header/Header";
import Footer from "../../components/footer/Footer";
import Button from "../../components/Button";
import OrderItem from "../../components/OrderItem";

Cart.propTypes = {};

function Cart(props) {
  return (
    <>
      <section className="cart-main">
        <section className="navigate">
          <a href="/">Home</a> / <a href="#">Shop</a> /{" "}
          <a href="#">Whishlist</a>
        </section>
        <section className="cart-content">
          <h1 className="title">Shopping Cart</h1>
          <section className="cart-title">
            <aside className="name-product">
              <p>PRODUCT NAME</p>
            </aside>
            <aside className="price-product">
              <p>PRICE</p>
            </aside>
            <aside>
              <p>QUANTITY</p>
            </aside>
            <aside>
              <p>TOTAL</p>
            </aside>
            <aside></aside>
          </section>
          <OrderItem />
          <OrderItem />
          <section className="back-update-button">
            <Button name={"Back to Shop"} />
            <Button name={"Update cart"} />
          </section>
          <section className="checkout-cart">
            <aside className="proceed">
              <section className="proceed-title">
                <p className="proceed-title-cart">Subtotal</p>
                <p className="proceed-title-cart">đ150,000,00</p>
              </section>
              <hr
                style={{
                  marginBlockEnd: 0,
                  marginBlockStart: 0,
                  width: "100%",
                  color: "#e2e2e2",
                }}
              />
              {/* ============================= */}
              <section className="proceed-content">
                <p className="proceed-title-cart">YOUNG SHOP Shipping</p>
                <p className="proceed-title-cart content-sub">Free Shipping</p>
                <p className="proceed-title-cart content-sub">
                  Estimate for Viet Nam
                </p>
              </section>

              {/* ============================= */}
              <section className="proceed-totalmoney">
                <p className="proceed-title-cart total-money">
                  <strong>Total</strong>
                </p>
                <p className="proceed-title-cart total-money">
                  <strong style={{ color: "red" }}>đ150,000,00</strong>
                </p>
              </section>
              <Button name={"Proceed to Checkout"} />
            </aside>
            <Button name={"Proceed to Checkout"} />
          </section>
        </section>
      </section>
    </>
  );
}

export default Cart;
