import React from "react";
import PropTypes from "prop-types";
import "../Account.css";

Notification.propTypes = {};

function Notification(props) {
  return (
    <section className="user-infor">
      <h2 className="user-infor-title">Notifications</h2>
      <table style={{ width: "100%", height: "auto" }} border={1}>
        <thead>
          <th style={{ width: "50%", height: 40 }}>Title</th>
          <th style={{ width: "20%", height: 40 }}>Date</th>
          <th style={{ width: "20%", height: 40 }}>Tag</th>
        </thead>
        <tbody>
          <tr key="">
            <td
              style={{
                width: "50%",
                fontSize: 13,
                textAlign: "left",
                padding: 10,
              }}
            >
              Lorem imkum dolor sit amet, consectetuer adipiscing elit. Aenean
              commodo ligula eget dolor
            </td>
            <td>20-1-2020</td>
            <td>
              <span
                style={{
                  background: "#2de92d",
                  color: "white",
                  padding: 5,
                  borderRadius: 6,
                  textAlign: "center",
                }}
              >
                new
              </span>
            </td>
          </tr>
          <tr key="">
            <td
              style={{
                width: "50%",
                fontSize: 13,
                textAlign: "left",
                padding: 10,
              }}
            >
              Lorem imkum dolor sit amet, consectetuer adipiscing elit. Aenean
              commodo ligula eget dolor
            </td>
            <td>20-1-2020</td>
            <td>
              <span>sale</span>
            </td>
          </tr>
        </tbody>
      </table>
    </section>
  );
}

export default Notification;
