import React from "react";
import PropTypes from "prop-types";
import "../Account.css";
import Button from "../../../components/Button";

UserInformation.propTypes = {};

function UserInformation(props) {
  return (
    <section className="user-infor">
      <h2 className="user-infor-title">User Information</h2>
      <form action="" method="get" className="user-infor-form">
        <section className="label-input">
          <label className="label">Name</label>
          <input
            className="input"
            type="text"
            placeholder="Please enter your name..."
          />
        </section>
        <section className="section-2">
          <aside className="label-input-left">
            <label className="label">Phone Number</label>
            <input
              className="input"
              type="text"
              placeholder="Please enter your phone number..."
            />
          </aside>
          <aside className="label-input-right">
            <label className="label">Email</label>
            <input
              className="input"
              type="text"
              placeholder="Please enter your email..."
            />
          </aside>
        </section>
        <section className="section-2">
          <aside className="label-input-left">
            <label className="label">Birthday</label>
            <input
              className="input"
              type="text"
              placeholder="Please enter your birthday..."
            />
          </aside>
          <aside className="label-input-right">
            <label className="label">Gender</label>
            <input
              className="input"
              type="text"
              placeholder="Please enter your email..."
            />
          </aside>
        </section>
        <Button name={"Update"} />
      </form>
    </section>
  );
}

export default UserInformation;
