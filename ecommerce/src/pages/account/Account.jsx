import React from "react";
import PropTypes from "prop-types";
import "./Account.css";
import { CiUser } from "react-icons/ci";
import { Outlet } from "react-router-dom";
Account.propTypes = {};

function Account(props) {
  return (
    <>
      <section className="cart-main">
        <section className="navigate">
          <a href="/">Home</a> / <a href="#">User Information</a>
        </section>
        <section className="account-content">
          <aside className="sidebar-account">
            <section className="account-header">
              <img
                src="/src/assets/images/product/iphone-11-promax.jpg"
                alt=".jpg"
                className="account-header-image"
              />
              <aside className="account-header-text">
                <p>Hello</p>
                <p>[Email protected]</p>
              </aside>
            </section>
            <ul className="list-account">
              <li className="list-account-item color-item">
                <CiUser style={{ fontSize: 18, color: "white" }} />
                <a href="user-infor/">Account Information</a>
              </li>
              <li className="list-account-item">
                <CiUser style={{ fontSize: 18, color: "white" }} />
                <a href="notification/">Notifications</a>
              </li>
              <li className="list-account-item">
                <CiUser style={{ fontSize: 18, color: "white" }} />
                <a href="#">Address</a>
              </li>
              <li className="list-account-item">
                <CiUser style={{ fontSize: 18, color: "white" }} />
                <a href="#">Logout</a>
              </li>
            </ul>
          </aside>
          {/* <div id="detail">
            <Outlet />
          </div> */}
          <Outlet />
        </section>
      </section>
    </>
  );
}

export default Account;
