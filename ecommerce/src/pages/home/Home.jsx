import React, { useState } from "react";
import PropTypes from "prop-types";
import Button from "../../components/Button";
import CardProduct from "../../components/CardProduct";
import CardProductList from "../../components/CardProductList";
//import icon
import { BsGrid3X3 } from "react-icons/bs";
import { FaListUl } from "react-icons/fa";
import { Carousel } from "react-responsive-carousel";

Home.propTypes = {};

function Home(props) {
  const [onChangeGridView, setOnChangeGridView] = useState(false);
  const handleOnChangeGridView = () => {
    setOnChangeGridView(true);
  };
  const handleOnChangeListView = () => {
    setOnChangeGridView(false);
  };
  return (
    <main className="content">
      <section className="navigate">
        <a href="#">Home</a> / <a href="#">Shop</a>
      </section>
      <section className="main">
        <section className="carousel">
          <span>{"<"}</span>
          <aside className="carousel-content">
            <p className="carousel-content-p title-version">VERSION 2021</p>
            <h2 className="carousel-content-p text-sale">50% discount</h2>
            <p className="carousel-content-p">
              New version 2021 with many powerfull features
            </p>
            <p className="carousel-content-p">
              Faster, Friction better & Cheap price
            </p>
            <Button />
          </aside>
          <span>{">"}</span>
        </section>
        <section className="main-content">
          <aside className="category-filter-aside">
            <main className="category-list"></main>
            <main className="filter"></main>
          </aside>
          <main className="product-content">
            <section className="title-slider">
              <h2 className="title-slider-h2">Best Sale Items</h2>
              <span>{"< >"}</span>
            </section>
            <hr
              style={{
                marginBlockEnd: 0,
                marginBlockStart: 0,
                width: "100%",
                color: "#e2e2e2",
              }}
            />

            <section className="slider-container">
              <CardProduct sale_text="7%" rate_number="01" />
              <CardProduct sale_text="7%" rate_number="01" />
              <CardProduct sale_text="7%" rate_number="01" />
              <CardProduct sale_text="7%" rate_number="01" />
            </section>

            <section className="title-slider">
              <h2 className="title-slider-h2">Recommended Items</h2>
              <span>{"< >"}</span>
            </section>
            <hr
              style={{
                marginBlockEnd: 0,
                marginBlockStart: 0,
                width: "100%",
                color: "#e2e2e2",
              }}
            />
            <section className="slider-container">
              <CardProduct sale_text="7%" rate_number="01" />
              <CardProduct sale_text="7%" rate_number="01" />
              <CardProduct sale_text="7%" rate_number="01" />
              <CardProduct sale_text="7%" rate_number="01" />
              <CardProduct sale_text="7%" rate_number="01" />
            </section>
            <br />
            <section className="sort-view-product">
              <p className="number-product">
                <strong>36</strong> Products found
              </p>
              <ul className="view-product">
                <li>
                  <strong>View:</strong>
                </li>
                <li onClick={handleOnChangeGridView}>
                  <BsGrid3X3 />
                </li>
                <li onClick={handleOnChangeListView}>
                  <FaListUl />
                </li>
              </ul>
            </section>
            <section className="view-card-product">
              {onChangeGridView ? (
                <><CardProduct sale_text="7%" rate_number="01" />
                  <CardProduct sale_text="7%" rate_number="01" />
                  <CardProduct sale_text="7%" rate_number="01" />
                </>
              ) : (
                <><CardProductList /><CardProductList /><CardProductList /></>
              )}
            </section>
          </main>
        </section>
      </section>
    </main>
  );
}

export default Home;
