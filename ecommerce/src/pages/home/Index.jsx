import React, { useState } from "react";
import PropTypes from "prop-types";
import "./Index.css";
import Header from "../../components/header/Header";
import Footer from "../../components/footer/Footer";
import { Outlet } from "react-router-dom";

Index.propTypes = {};

function Index(props) {
  const [onChangeGridView, setOnChangeGridView] = useState(false);
  const handleOnChangeGridView = () => {
    setOnChangeGridView(true);
  };
  const handleOnChangeListView = () => {
    setOnChangeGridView(false);
  };
  return (
    <>
      <Header />
      <div id="detail">
        <Outlet />
      </div>
      <Footer />
    </>
  );
}

export default Index;
