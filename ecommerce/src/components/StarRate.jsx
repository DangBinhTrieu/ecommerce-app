import React from "react";
import PropTypes from "prop-types";
import "./style.css";
import { FaStar } from "react-icons/fa6";

StarRate.propTypes = {};

function StarRate({ rate_number }) {
  return (
    <section
      className="star-rate"
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        height: 20,
        display: rate_number !== "" ? "flex" : "none",
      }}
    >
      <ul className="list-start">
        <li>
          <FaStar style={{ color: "yellow", height: 15 }} />
        </li>
        <li>
          <FaStar style={{ color: "yellow", height: 15 }} />
        </li>
        <li>
          <FaStar style={{ color: "yellow", height: 15 }} />
        </li>
        <li>
          <FaStar style={{ color: "yellow", height: 15 }} />
        </li>
        <li>
          <FaStar style={{ color: "#d9d9d9", height: 15 }} />
        </li>
      </ul>
      <p className="number-rate">{rate_number}</p>
    </section>
  );
}

export default StarRate;
