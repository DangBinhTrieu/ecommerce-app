import React from "react";
import PropTypes from "prop-types";
import "./style.css";

Button.propTypes = {};

function Button({ name }) {
  return <input type="submit" value={name} className="btn-button" />;
}

export default Button;
