import React, { useState } from "react";
import PropTypes from "prop-types";
import "./style.css";
//Import icons
import { CiUser } from "react-icons/ci";
import { FiShoppingCart } from "react-icons/fi";
import { CiHeart } from "react-icons/ci";
import { IoBarChartOutline } from "react-icons/io5";
import StarRate from "./StarRate";

CardProduct.propTypes = {};

function CardProduct({ sale_text, rate_number }) {
  const [onHoverIcon, setOnHoverIcon] = useState(false);
  const handleOnHover = () => {
    setOnHoverIcon(!onHoverIcon);
  };
  return (
    <aside className="card-product">
      <section className="card-image">
        <img
          src="/src/assets/images/product/iphone-11-promax.jpg"
          className="image-prod"
          alt=".jpg"
        />
        <div className="show-icon">
          <ul className="list-icon-card">
            <li className="">
              <IoBarChartOutline
                style={{
                  fontSize: 16,
                  color: onHoverIcon ? "white" : "#6f6f6f",
                }}
              />
            </li>
            <li className="">
              <CiHeart
                style={{
                  fontSize: 16,
                  color: onHoverIcon ? "white" : "#6f6f6f",
                }}
              />
            </li>
            <li className="">
              <FiShoppingCart
                style={{
                  fontSize: 16,
                  color: onHoverIcon ? "white" : "#6f6f6f",
                }}
              />
            </li>
            <li>
              <CiUser
                style={{
                  fontSize: 16,
                  color: onHoverIcon ? "white" : "#6f6f6f",
                }}
              />
            </li>
          </ul>
        </div>
      </section>
      <section className="card-content">
        <p className="title-product">YOUNG SHOP</p>
        <hr
          style={{
            marginBlockEnd: 0,
            marginBlockStart: 0,
            width: "100%",
            color: "#e2e2e2",
          }}
        />
        <p className="title-product description-product">
          Marshall Kilburn Portable Wireless Speaker
        </p>
        <StarRate rate_number={rate_number} />
        <p className="title-product price-product">đ1,500,000</p>
        <p className="title-product price-product older">đ1,800,000</p>
      </section>
      <span
        className="sale-product"
        style={{
          display: sale_text !== "" ? "block" : "none",
        }}
      >
        {sale_text}
      </span>
    </aside>
  );
}

export default CardProduct;
