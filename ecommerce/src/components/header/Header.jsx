import React from "react";
import PropTypes from "prop-types";
import "./Header.css";
//Import icons
import { CiUser } from "react-icons/ci";
import { FiShoppingCart } from "react-icons/fi";
import { CiHeart } from "react-icons/ci";
import { IoBarChartOutline } from "react-icons/io5";

Header.propTypes = {};

function Header(props) {
  return (
    <header className="header" style={{}}>
      <form action="" className="form-submit">
        <input
          type="text"
          placeholder="Search for items..."
          name="searchText"
          className="searchText"
        />
        <input type="submit" className="searchButton" value="Search" />
      </form>
      <ul className="list-icon">
        <li className="list-icon-item">
          <IoBarChartOutline style={{ fontSize: 24, color: "white" }} />
        </li>
        <li className="list-icon-item">
          <CiHeart style={{ fontSize: 24, color: "white" }} />
        </li>
        <li className="list-icon-item">
          <a href="/cart/">
            <FiShoppingCart style={{ fontSize: 24, color: "white" }} />
          </a>
        </li>
        <li>
          <a href="/account/">
            <CiUser style={{ fontSize: 24, color: "white" }} />
          </a>
        </li>
        <li className="list-icon-text">
          <a href="#">Login</a>/<a href="#">Register</a>
        </li>
      </ul>
    </header>
  );
}

export default Header;
