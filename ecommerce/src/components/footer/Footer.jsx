import React from "react";
import PropTypes from "prop-types";
import "./Footer.css";

Footer.propTypes = {};

function Footer(props) {
  return (
    <footer className="footer">
      <hr
        style={{
          marginBlockEnd: 0,
          marginBlockStart: 0,
          width: "90%",
          color: "#e2e2e2",
        }}
      />
      <section className="footer-top">
        <aside className="title">
          <h2>Newsletter</h2>
          <h6>Subcribe to get information about products and coupons</h6>
        </aside>
        <main className="form-input">
          <input
            type="text"
            placeholder="Email Address"
            name="emailText"
            className="emailText"
          />
          <input type="submit" value={"Subcribe"} className={"subcribe-btn"} />
        </main>
      </section>
      <hr
        style={{
          marginBlockEnd: 0,
          marginBlockStart: 0,
          width: "90%",
          color: "#e2e2e2",
        }}
      />
      <section className="footer-middle">
        <main className="contact">
          <p className="contact-p">Contact us</p>
          <span className="contact-span">Call us 24/7</span>
          <h2>120 000 000 99</h2>
          <span className="contact-span " style={{ textAlign: "left" }}>
            100 Fifth Avenue New York, USA info@example.com
          </span>
        </main>
        <aside className="link-list">
          <p className="contact-p">Quick links</p>
          <ul className="link-list-ul">
            <li className="link-list-item">
              <a href="#">Policy</a>
            </li>
            <li className="link-list-item">
              <a href="#">Term & Condition</a>
            </li>
            <li className="link-list-item">
              <a href="#">Shipping</a>
            </li>
            <li className="link-list-item">
              <a href="#">Return</a>
            </li>
            <li className="link-list-item">
              <a href="#">FAQs</a>
            </li>
          </ul>
        </aside>
        <aside className="link-list">
          <p className="contact-p">Company</p>
          <ul className="link-list-ul">
            <li className="link-list-item">
              <a href="#">About us</a>
            </li>
            <li className="link-list-item">
              <a href="#">Affilate</a>
            </li>
            <li className="link-list-item">
              <a href="#">Career</a>
            </li>
            <li className="link-list-item">
              <a href="#">Contact</a>
            </li>
          </ul>
        </aside>
        <aside className="link-list">
          <p className="contact-p">Business</p>
          <ul className="link-list-ul">
            <li className="link-list-item">
              <a href="#">Our Press</a>
            </li>
            <li className="link-list-item">
              <a href="#">Check Out</a>
            </li>
            <li className="link-list-item">
              <a href="#">My Account</a>
            </li>
            <li className="link-list-item">
              <a href="#">Shop</a>
            </li>
          </ul>
        </aside>
      </section>
      <hr
        style={{
          marginBlockEnd: 0,
          marginBlockStart: 0,
          width: "90%",
          color: "#e2e2e2",
        }}
      />
      <section className="footer-bottom"></section>
    </footer>
  );
}

export default Footer;
