import React from "react";
import PropTypes from "prop-types";

OrderItem.propTypes = {};

function OrderItem(props) {
  return (
    <section className="cart-order">
      <aside className="name-product">
        <img
          src="/src/assets/images/product/iphone-11-promax.jpg"
          alt=".jpg"
          className="image-cart"
        />
        <main className="name-product-text">
          <p className="title-product  description-product title-product-cart">
            Marshall Kilburn Portable Wireless Speaker
          </p>
          <p className="title-product title-product-cart">YOUNG SHOP</p>
        </main>
      </aside>
      <aside className="">
        {" "}
        <p>đ150,000,00</p>
      </aside>
      <aside className="quantity-btn">
        <input className="btn" type="submit" value="-" />
        <p>01</p>
        <input className="btn" type="submit" value="+" />
      </aside>
      <aside>
        <p>đ150,000,00</p>
      </aside>
      <aside>X</aside>
    </section>
  );
}

export default OrderItem;
