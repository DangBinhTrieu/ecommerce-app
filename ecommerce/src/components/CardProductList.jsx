import React from "react";
import PropTypes from "prop-types";
import "./style.css";
import Button from "./Button";

CardProductList.propTypes = {};

function CardProductList(props) {
  return (
    <section className="card-product-list">
      <aside className="card-image-list">
        <img
          src="/src/assets/images/product/iphone-11-promax.jpg"
          className="image-prod-list"
          alt=".jpg"
        />
      </aside>
      <aside className="card-content-list">
        <p className="title-product description-product">
          Marshall Kilburn Portable Wireless Speaker
        </p>
        <p className="title-product">Sold by:ROBERT'S STORE</p>
        <ul className="list-description-product">
          <li className="list-description-product-item">
            Unrestrained and portable active stereo speaker
          </li>
          <li className="list-description-product-item">
            Free from the confines of wires and chords
          </li>
          <li className="list-description-product-item">
            20 hours of portable capabilities
          </li>
          <li className="list-description-product-item">
            Double-ended Coil Cord with 3.5mm Stereo Plugs Included
          </li>
          <li className="list-description-product-item">
            3/4″ Dome Tweeters: 2X and 4″ Woofer: 1X
          </li>
        </ul>
      </aside>
      <aside className="card-button">
        <p>
          <strong>đ7,000,000</strong>
        </p>
        <Button name="Add to cart" />
      </aside>
    </section>
  );
}

export default CardProductList;
