import React from "react";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import ErrorPage from "../router/ErrorPage";
import Root from "../router/Root";
import Console from "../screens/console/Console";
import Customer from "../screens/customer/Customer";
import Order from "../screens/order/Order";
import Product from "../screens/product/Product";
import CreateStaff from "../screens/staff/CreateStaff";
import Staff from "../screens/staff/Staff";

// const router = createBrowserRouter([
//   createRoutesFromElements(
//     <Router
//       path="/"
//       element={<Root />}
//       // loader={rootLoader}
//       // action={rootAction}
//       errorElement={<ErrorPage />}
//     >
//       <Route errorElement={<ErrorPage />}>
//         <Route index element={<Index />} />
//         <Route
//           path="contacts/:contactId"
//           element={<Contact />}
//           loader={contactLoader}
//           action={contactAction}
//         />
//         <Route
//           path="contacts/:contactId/edit"
//           element={<EditContact />}
//           loader={contactLoader}
//           action={editAction}
//         />
//         <Route path="contacts/:contactId/destroy" action={destroyAction} />
//       </Route>
//     </Router>
//   ),
// ]);
const router = createBrowserRouter([
  {
    path: "/",
    element: <Root />,
    errorElement: <ErrorPage />,
    children: [
      {
        path: "/console",
        element: <Console />,
      },
      {
        path: "/customer",
        element: <Customer />,
      },
      {
        path: "/staff",
        element: <Staff />,
      },
      {
        path: "/product",
        element: <Product />,
      },
      {
        path: "/order",
        element: <Order />,
      },
    ],
  },
]);

function Navigation(props) {
  return <RouterProvider router={router} />;
}

export default Navigation;
