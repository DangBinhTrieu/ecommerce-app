import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import "./Staff.css";
import ListButton from "../../components/ListButton";
import Pagination from "../../components/Pagination";
import FilterableTable from "../../components/FilterableTable";
import CreateStaff from "./CreateStaff";
import { titleThOfStaff } from "../../components/model-data/titleTh";
import { customers, staffs } from "../../components/model-data/data";

Staff.propTypes = {};

function Staff(props) {
  useEffect(() => {
    setInterval(() => {
      setDate(new Date());
    }, 1_000);
  }, []);

  const [date, setDate] = useState(new Date());
  const [showCreation, setShowCreation] = useState(false);
  function handleShowCreation() {
    setShowCreation(!showCreation);
  }
  return (
    <>
      {showCreation ? (
        <CreateStaff handleShowCreation={handleShowCreation} />
      ) : (
        <div id="staff">
          <div className="title">
            <p className="title-text">Danh sách nhân viên</p>
            <p className="title-timer">
              {date.toLocaleTimeString()} - {date.toLocaleDateString()}
            </p>
          </div>
          <main className="main-staff">
            <ListButton
              addNewButtonName={"Thêm mới nhân viên"}
              handleShowCreation={handleShowCreation}
            />
            <hr
              style={{
                border: "1px solid #ff7a26",
                display: "inline",
                width: "100%",
              }}
            />
            <FilterableTable
              titleThArray={titleThOfStaff}
              message={"Không có tìm thấy nhân viên nào"}
              data={staffs}
            />
            <Pagination />
          </main>
        </div>
      )}
    </>
  );
}

export default Staff;
