import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import "./Staff.css";
import ButtonSubmit from "../../components/ButtonSubmit";
import GroupLabelInput from "../../components/GroupLabelInput";
import Modal from "../../components/Modal";
import { supplierDatas } from "../../components/model-data/supplierData";

CreateStaff.propTypes = {};

function CreateStaff({ handleShowCreation }) {
  useEffect(() => {
    setInterval(() => {
      setDate(new Date());
    }, 1_000);
  }, []);

  const [date, setDate] = useState(new Date());
  const [showModal, setShowModal] = useState(false);
  function handleShowModal() {
    setShowModal(!showModal);
  }
  return (
    <>
      <div id="staff">
        <div className="title">
          <p className="title-text">Danh sách nhân viên / Thêm nhân viên mới</p>
          <p className="title-timer">
            {date.toLocaleTimeString()} - {date.toLocaleDateString()}
          </p>
        </div>
        <main className="main-staff">
          <section
            style={{
              width: "98%",
              display: "flex",
              flexDirection: "column",
              alignItems: "flex-start",
              justifyContent: "center",
              position: "absolute",
              left: 10,
            }}
          >
            <p
              style={{
                marginBlockStart: 0,
                marginBlockEnd: 0,
                fontSize: 14,
                fontWeight: "bold",
              }}
            >
              Thêm mới nhân viên
            </p>
            <hr
              style={{
                border: "1px solid #ff7a26",
                display: "inline",
                width: "100%",
              }}
            />
            <button
              className="button-top"
              style={{ background: "#5ff25f" }}
              onClick={handleShowModal}
            >
              <i class="fa fa-plus" aria-hidden="true"></i>
              Tạo chức vụ mới
            </button>
            <hr
              style={{
                border: "1px solid #ececec",
                display: "inline",
                width: "100%",
              }}
            />
          </section>
          <hr style={{ borderWidth: 2 }} />
          <section className="section-input-group">
            <GroupLabelInput>
              <label className="label">Họ và Tên</label>
              <input className="input" type="text" />
            </GroupLabelInput>
            <GroupLabelInput>
              <label className="label">Địa chỉ email</label>
              <input className="input" type="text" />
            </GroupLabelInput>
            <GroupLabelInput>
              <label className="label">Địa chỉ thường trú</label>
              <input className="input" type="text" />
            </GroupLabelInput>
            <GroupLabelInput>
              <label className="label">Số điện thoại</label>
              <input className="input" type="text" />
            </GroupLabelInput>
            <GroupLabelInput>
              <label className="label">Ngày sinh</label>
              <input className="input" type="date" />
            </GroupLabelInput>
            <GroupLabelInput>
              <label className="label">Nơi sinh</label>
              <input className="input" type="text" />
            </GroupLabelInput>
            <GroupLabelInput>
              <label className="label">Số CMND</label>
              <input className="input" type="text" />
            </GroupLabelInput>
            <GroupLabelInput>
              <label className="label">Ngày cấp</label>
              <input className="input" type="date" />
            </GroupLabelInput>
            <GroupLabelInput>
              <label className="label">Nới cấp</label>
              <input className="input" type="text" />
            </GroupLabelInput>
            <GroupLabelInput>
              <label className="label">Giới tính</label>
              <select className="select">
                <option value="">- Chọn giới tính -</option>
                <option value="">Nam</option>
                <option value="">Nữ</option>
                <option value="">Khác</option>
              </select>
            </GroupLabelInput>
            <GroupLabelInput>
              <label className="label">Chức vụ</label>
              <select className="select">
                <option value="">- Chọn chức vụ -</option>
                <option value="">Nhân viên bán hàng</option>
                <option value="">Nhân viên kiểm kho</option>
                <option value="">Nhân viên tiếp thị</option>
              </select>
            </GroupLabelInput>
            <GroupLabelInput>
              <label className="label">Bằng cấp</label>
              <select className="select">
                <option value="">- Chọn bằng cấp -</option>
                <option value="">Đại học/Cao đẵng</option>
                <option value="">Trung học phổ thông</option>
                <option value="">Sơ cấp</option>
              </select>
            </GroupLabelInput>
            <GroupLabelInput>
              <label className="label">Tình trạng hôn nhân</label>
              <select className="select">
                <option value="">- Chọn tình trạng hôn nhân -</option>
                <option value="">Độc thân</option>
                <option value="">Đã kết hôn</option>
              </select>
            </GroupLabelInput>
            <GroupLabelInput>
              <label className="label">Ảnh 3x4 của nhân viên</label>
              <input
                className="input"
                id="upload"
                type="file"
                style={{ width: 200 }}
                hidden
              />
              <label className="label-choose-file" for="upload">
                Choose file
              </label>
            </GroupLabelInput>
          </section>
          <ButtonSubmit onClickChange={handleShowCreation} />
        </main>
      </div>
      {showModal && (
        <Modal
          titleOfModal={"Thêm chức vụ"}
          nameInputOfModal={"Thêm mới chức vụ"}
          nameListOfModal={"Danh sách chức vụ đang có"}
          messageNotFoundData={"Không có chức vụ nào được lưu"}
          datas={supplierDatas}
          onClickChange={handleShowModal}
        />
      )}
    </>
  );
}

export default CreateStaff;
