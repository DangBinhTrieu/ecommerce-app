import React, { useState } from "react";
import PropTypes from "prop-types";

RowEditOfProduct.propTypes = {};

function RowEditOfProduct({ item, id, onCancel }) {
  const [editingData, setEditingData] = useState(item);
  return (
    <tr key={item.id}>
      <td className="checkbox">
        <input type="checkbox" value={item.id} />
      </td>
      <td>
        <input
          type="text"
          value={editingData.id}
          onChange={(e) =>
            setEditingData((item) => ({ ...editingData, id: e.target.value }))
          }
          style={{ width: "60px", height: "20px" }}
        />
      </td>
      <td>
        <input
          type="text"
          value={editingData.name}
          onChange={(e) =>
            setEditingData((item) => ({ ...editingData, name: e.target.value }))
          }
          style={{ width: "60px", height: "20px" }}
        />
      </td>
      <td>
        <input
          type="text"
          value={editingData.image}
          onChange={(e) =>
            setEditingData((item) => ({
              ...editingData,
              image: e.target.value,
            }))
          }
          style={{ width: "60px", height: "20px" }}
        />
      </td>
      <td>
        <input
          type="text"
          value={editingData.quantity}
          onChange={(e) =>
            setEditingData((item) => ({
              ...editingData,
              quantity: e.target.value,
            }))
          }
          style={{ width: "60px", height: "20px" }}
        />
      </td>
      <td>
        <input
          type="text"
          value={editingData.status}
          onChange={(e) =>
            setEditingData((item) => ({
              ...editingData,
              status: e.target.value,
            }))
          }
          style={{ width: "60px", height: "20px" }}
        />
      </td>
      <td>
        <input
          type="text"
          value={editingData.price}
          onChange={(e) =>
            setEditingData((item) => ({
              ...editingData,
              price: e.target.value,
            }))
          }
          style={{ width: "60px", height: "20px" }}
        />
      </td>
      <td>
        <input
          type="text"
          value={editingData.sale}
          onChange={(e) =>
            setEditingData((item) => ({
              ...editingData,
              sale: e.target.value,
            }))
          }
          style={{ width: "60px", height: "20px" }}
        />
      </td>
      <td>
        <input
          type="text"
          value={editingData.category}
          onChange={(e) =>
            setEditingData((item) => ({
              ...editingData,
              category: e.target.value,
            }))
          }
          style={{ width: "60px", height: "20px" }}
        />
      </td>
      <td className="action">
        <button className="button-top" style={{ background: "#5ff25f" }}>
          <i class="fa fa-plus" aria-hidden="true"></i>
          Lưu
        </button>
        <button
          className="button-top"
          style={{ background: "#ff7d7d" }}
          onClick={onCancel}
        >
          <i class="fa fa-plus" aria-hidden="true"></i>
          Hủy
        </button>
      </td>
    </tr>
  );
}

export default RowEditOfProduct;
