import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import "./Product.css";
import ListButton from "../../components/ListButton";
import FilterableTable from "../../components/FilterableTable";
import Pagination from "../../components/Pagination";
import CreateProduct from "./CreateProduct";
import { titleThOfProduct } from "../../components/model-data/titleTh";
import { products, staffs } from "../../components/model-data/data";

Product.propTypes = {};

function Product(props) {
  useEffect(() => {
    setInterval(() => {
      setDate(new Date());
    }, 1_000);
  }, []);

  const [date, setDate] = useState(new Date());
  const [showCreation, setShowCreation] = useState(false);
  function handleShowCreation() {
    setShowCreation(!showCreation);
  }
  return (
    <>
      {showCreation ? (
        <CreateProduct handleShowCreation={handleShowCreation} />
      ) : (
        <div id="product">
          <div className="title">
            <p className="title-text">Danh sách sản phẩm</p>
            <p className="title-timer">
              {date.toLocaleTimeString()} - {date.toLocaleDateString()}
            </p>
          </div>
          <main className="main-product">
            <ListButton
              addNewButtonName={"Thêm mới sản phẩm"}
              handleShowCreation={handleShowCreation}
            />
            <hr
              style={{
                border: "1px solid #ff7a26",
                display: "inline",
                width: "100%",
              }}
            />
            <FilterableTable
              titleThArray={titleThOfProduct}
              message={"Không có tìm thấy sản phẩm nào"}
              product={products}
            />
            <Pagination />
          </main>
        </div>
      )}
    </>
  );
}

export default Product;
