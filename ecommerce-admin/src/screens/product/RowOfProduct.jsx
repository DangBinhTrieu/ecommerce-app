import React from "react";
import PropTypes from "prop-types";
import MessageStatus from "../../components/MessageStatus";

RowOfProduct.propTypes = {};

function RowOfProduct({ item, handleEditClick }) {
  return (
    <tr key={item.id}>
      <td className="checkbox">
        <input type="checkbox" value={item.id} />
      </td>
      <td>{item.id}</td>
      <td>{item.name}</td>
      <td>{item.image}</td>
      <td>{item.quantity}</td>
      <td>
        {item.status ? (
          <MessageStatus bgColor={"green"}>Còn hàng</MessageStatus>
        ) : (
          <MessageStatus bgColor={"red"}>Đã hết hàng</MessageStatus>
        )}
      </td>
      <td>{`${item.price}đ`}</td>
      <td>{`${(item.sale * item.price) / 100}đ`}</td>
      <td>{item.category}</td>
      <td className="action">
        <button
          className="button-top"
          style={{ background: "#cf4eff" }}
          onClick={handleEditClick}
        >
          <i class="fa fa-plus" aria-hidden="true"></i>
          Edit
        </button>
        <button className="button-top" style={{ background: "#ff7d7d" }}>
          <i class="fa fa-plus" aria-hidden="true"></i>
          Del
        </button>
      </td>
    </tr>
  );
}

export default RowOfProduct;
