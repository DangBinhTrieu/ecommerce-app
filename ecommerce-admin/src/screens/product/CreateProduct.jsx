import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import "./Product.css";
import ButtonSubmit from "../../components/ButtonSubmit";
import GroupLabelInput from "../../components/GroupLabelInput";
import Modal from "../../components/Modal";
import { supplierDatas } from "../../components/model-data/supplierData";

CreateProduct.propTypes = {};

function CreateProduct({ handleShowCreation }) {
  useEffect(() => {
    setInterval(() => {
      setDate(new Date());
    }, 1_000);
  }, []);

  const [date, setDate] = useState(new Date());
  const [showModalOfSupplier, setShowModalOfSupplier] = useState(false);
  const [showModalOfCategory, setShowModalOfCategory] = useState(false);
  const [showModalOfStatus, setShowModalOfStatus] = useState(false);
  function handleShowModalOfSupplier() {
    setShowModalOfSupplier(!showModalOfSupplier);
  }
  function handleShowModalOfCategory() {
    setShowModalOfCategory(!showModalOfCategory);
  }
  function handleShowModalOfStatus() {
    setShowModalOfStatus(!showModalOfStatus);
  }
  return (
    <>
      <div id="product">
        <div className="title">
          <p className="title-text">Danh sách sản phẩm / Thêm sản phẩm mới</p>
          <p className="title-timer">
            {date.toLocaleTimeString()} - {date.toLocaleDateString()}
          </p>
        </div>
        <main className="main-product">
          <section
            style={{
              width: "98%",
              display: "flex",
              flexDirection: "column",
              alignItems: "flex-start",
              justifyContent: "center",
              position: "absolute",
              left: 10,
            }}
          >
            <p
              style={{
                marginBlockStart: 0,
                marginBlockEnd: 0,
                fontSize: 14,
                fontWeight: "bold",
              }}
            >
              Thêm mới sản phẩm
            </p>
            <hr
              style={{
                border: "1px solid #ff7a26",
                display: "inline",
                width: "100%",
              }}
            />
            <section
              style={{
                width: 400,
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "space-between",
              }}
            >
              <button
                className="button-top"
                style={{ background: "#5ff25f" }}
                onClick={handleShowModalOfSupplier}
              >
                <i class="fa fa-plus" aria-hidden="true"></i>
                Thêm nhà cung cấp
              </button>
              <button
                className="button-top"
                style={{ background: "#5ff25f" }}
                onClick={handleShowModalOfCategory}
              >
                <i class="fa fa-plus" aria-hidden="true"></i>
                Thêm danh mục
              </button>
              <button
                className="button-top"
                style={{ background: "#5ff25f" }}
                onClick={handleShowModalOfStatus}
              >
                <i class="fa fa-plus" aria-hidden="true"></i>
                Thêm tình trạng
              </button>
            </section>
            <hr
              style={{
                border: "1px solid #ececec",
                display: "inline",
                width: "100%",
              }}
            />
          </section>
          <hr style={{ borderWidth: 2 }} />
          <section className="section-input-group">
            <GroupLabelInput>
              <label className="label">Tên sản phẩm</label>
              <input className="input" type="text" />
            </GroupLabelInput>
            <GroupLabelInput>
              <label className="label">Sales</label>
              <input className="input" type="text" />
            </GroupLabelInput>
            <GroupLabelInput>
              <label className="label">Giá bán</label>
              <input className="input" type="text" />
            </GroupLabelInput>
            <GroupLabelInput>
              <label className="label">Giá vốn</label>
              <input className="input" type="text" />
            </GroupLabelInput>
            <GroupLabelInput>
              <label className="label">Số lượng</label>
              <input className="input" type="text" />
            </GroupLabelInput>
            <GroupLabelInput>
              <label className="label">Tình trạng</label>
              <select className="select">
                <option value="">- Chọn tình trạng -</option>
                <option value="">Nhân viên bán hàng</option>
                <option value="">Nhân viên kiểm kho</option>
                <option value="">Nhân viên tiếp thị</option>
              </select>
            </GroupLabelInput>
            <GroupLabelInput>
              <label className="label">Danh mục</label>
              <select className="select">
                <option value="">- Chọn danh mục -</option>
                <option value="">Đại học/Cao đẵng</option>
                <option value="">Trung học phổ thông</option>
                <option value="">Sơ cấp</option>
              </select>
            </GroupLabelInput>
            <GroupLabelInput>
              <label className="label">Nhà cung cấp</label>
              <select className="select">
                <option value="">- Chọn nhà cung cấp -</option>
                <option value="">Độc thân</option>
                <option value="">Đã kết hôn</option>
              </select>
            </GroupLabelInput>
            <GroupLabelInput>
              <label className="label">Ảnh sản phẩm</label>
              <input
                className="input"
                id="upload"
                type="file"
                style={{ width: 200 }}
                hidden
              />
              <label className="label-choose-file" for="upload">
                Choose file
              </label>
            </GroupLabelInput>
            <GroupLabelInput>
              <label className="label">Mô tả sản phẩm</label>
              <input className="input" type="text" />
            </GroupLabelInput>
          </section>
          <ButtonSubmit onClickChange={handleShowCreation} />
        </main>
      </div>
      {showModalOfSupplier && (
        <Modal
          titleOfModal={"Thêm nhà cung cấp sản phẩm"}
          nameInputOfModal={"Thêm mới nhà cung cấp sản phẩm"}
          nameListOfModal={"Danh sách nhà cung cấp sản phẩm đang có"}
          messageNotFoundData={"Không có nhà cung cấp nào"}
          datas={supplierDatas}
          onClickChange={handleShowModalOfSupplier}
        />
      )}
      {showModalOfCategory && (
        <Modal
          titleOfModal={"Thêm danh mục sản phẩm"}
          nameInputOfModal={"Thêm mới danh mục sản phảm"}
          nameListOfModal={"Danh sách danh mục sản phảm đang có"}
          messageNotFoundData={"Không có danh mục nào"}
          datas={supplierDatas}
          onClickChange={handleShowModalOfCategory}
        />
      )}
      {showModalOfStatus && (
        <Modal
          titleOfModal={"Thêm tình trạng sản phẩm"}
          nameInputOfModal={"Thêm mới tình trạng sản phảm"}
          nameListOfModal={"Danh sách tình trạng sản phảm đang có"}
          messageNotFoundData={"Không có trạng thái nào được lưu"}
          datas={supplierDatas}
          onClickChange={handleShowModalOfStatus}
        />
      )}
    </>
  );
}

export default CreateProduct;
