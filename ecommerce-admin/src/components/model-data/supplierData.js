export const supplierDatas = [
  {
    id: 1,
    name: "Nhà cung cấp và xuất bản Hà Nội",
    address: "Quận Nam Từ Liêm, Hà Nội",
    createAt: "12/02/2024",
  },
  {
    id: 2,
    name: "Nhà xuất bản Đà Nẵng",
    address: "Quận Hòa Vang, Đà Nẵng",
    createAt: "12/02/2024",
  },
  {
    id: 3,
    name: "Nhà sách Quảng Ngãi",
    address: "Thành phố Quảng Ngãi",
    createAt: "12/02/2024",
  },
];
