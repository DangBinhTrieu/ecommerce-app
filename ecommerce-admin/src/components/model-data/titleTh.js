export const titleThOfStaff = [
  { id: 1, name: "ID nhân viên" },
  { id: 2, name: "Họ và tên" },
  { id: 3, name: "Ảnh" },
  { id: 4, name: "Địa chỉ" },
  { id: 5, name: "Ngày sinh" },
  { id: 6, name: "Giới tính" },
  { id: 7, name: "SĐT" },
  { id: 8, name: "Chức vụ" },
];
export const titleThOfProduct = [
  { id: 1, name: "Mã sản phẩm" },
  { id: 2, name: "Tên sản phẩm" },
  { id: 3, name: "Ảnh" },
  { id: 4, name: "Số lượng" },
  { id: 5, name: "Tình trạng" },
  { id: 6, name: "Giá tiền" },
  { id: 7, name: "Giảm giá" },
  { id: 8, name: "Danh mục" },
];
export const titleThOfCustomer = [
  { id: 1, name: "ID khách hàng" },
  { id: 2, name: "Họ và tên" },
  { id: 3, name: "Ảnh" },
  { id: 4, name: "Địa chỉ" },
  { id: 5, name: "Ngày sinh" },
  { id: 6, name: "Giới tính" },
  { id: 7, name: "SĐT" },
  { id: 8, name: "Email" },
];
